from sanic import Sanic
import motor.motor_asyncio
import asyncio
import pprint


app = Sanic('appSanicMotor')
client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)

db = client.test_database

collection = db.test_collection

# insert
'''
async def do_insert():
    document = {'key': 'value'}
    result = await db.test_collection.insert_one(document)
    print('result %s' % repr(result.inserted_id))
'''

# usando coroutines
async def do_insert():
    for i in range(2000):
        result = await db.test_collection.insert_one({'i': i})

# getting a single document with find_one
async def do_find_one():
    document = await db.test_collection.find_one({'i': {'$lt': 1}})
    pprint.pprint(document)

# queryin for more than one document
async def do_find():
    cursor = db.test_collection.find({'i': {'$lt': 5}}).sort('i')
    for document in await cursor.to_list(length=100):
        pprint.pprint(document)

# async for
async def do_find2():
    c = db.test_collection
    async for document in c.find({'i': {'$lt': 2}}):
        pprint.pprint(document)

async def do_find3():
    cursor = db.test_collection.find({'i': {'$lt': 5}})
    cursor.sort('i', -1).limit(2).skip(2)
    async for document in cursor:
        pprint.pprint(document)

async def do_count():
    n = await db.test_collection.find().count()
    print('%s documents in collection' % n)
    n = await db.test_collection.find({'i': {'$gt': 1000}}).count()
    print('%s document where i > 1000' % n)

# updating documents
async def do_replace(): # atualizando chave e valor para aquele objeto (id)
    coll = db.test_collection
    old_document = await coll.find_one({'i': 50})
    print('found document: %s' % pprint.pformat(old_document))
    _id = old_document['_id']
    result = await coll.replace_one({'_id': _id}, {'key': 'value'})
    print('replaced %s document' % result.modified_count)
    new_document = await coll.find_one({'_id': _id})
    print('document is now %s' % pprint.pformat(new_document))

async def do_update(): # adicionar novos atributos para quele objeto (id)
    coll = db.test_collection
    result = await coll.update_one({'i': 51}, {'$set': {'key': 'value'}})
    print('updated %s document' % result.modified_count)
    new_document = await coll.find_one({'i': 51})
    print('document is now %s' % pprint.pformat(new_document))


# deleting documents
async def do_delete_many():
    coll = db.test_collection
    n = await coll.count()
    print('%s documents before calling delete_many()' % n)
    result = await db.test_collection.delete_many({'i': {'$gte': 1000}})
    print('%s documents after' % (await coll.count()))


loop = asyncio.get_event_loop()
# loop.run_until_complete(do_insert())
# loop.run_until_complete(do_find_one())
# loop.run_until_complete(do_find())
# loop.run_until_complete(do_find2())
# loop.run_until_complete(do_find3())
# loop.run_until_complete(do_count())
# loop.run_until_complete(do_replace())
# loop.run_until_complete(do_update())
loop.run_until_complete(do_delete_many())

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
