
# # http://masnun.rocks/2016/11/17/exploring-asyncio-uvloop-sanic-motor/

from sanic import Sanic
import motor.motor_asyncio
import asyncio
from sanic.response import json


app = Sanic('uvloop')

mongo_connection = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
contacts = mongo_connection.mydatabase.contacts

@app.route("/")
async def list(request):
    data = await contacts.find().to_list(20)
    for x in data:
        x['id'] = str(x['_id'])
        del x['_id']

    return json(data)


@app.route("/new")
async def new(request):
    contact = request.json
    insert = await contacts.insert_one(contact)
    return json({"inserted_id": str(insert.inserted_id)})


loop = asyncio.get_event_loop()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
