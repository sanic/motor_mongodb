## Getting Started - motor with asyncio

Seguindo o tutorial da documentação do motor com asyncio. Disponível na url: https://motor.readthedocs.io/en/stable/tutorial-asyncio.html

## Instalando dependências
- Crie uma virtualenv com python3 utilizando o seguinte comando: `python3 -m venv motor`
- Ative a virtualenv `motor`: `. motor/bin/activate`

- Instale as dependências contidas no arquivo requirements.txt: `pip install -r requirements.txt`

<!-- ## Executando o projeto
- Com a virtualenv ativa, execute o servidor: `python main.py`
- Para ver o resultado acesse a ulr: `http://0.0.0.0:8000` -->

