import asyncio

from aiohttp import web
from motor.motor_asyncio import AsyncIOMotorClient

# @asyncio.coroutine
async def setup_db():
    db = AsyncIOMotorClient().test
    # yield from db.pages.drop()
    await db.pages.drop()
    html = '<html><body>{}</body></html>'
    await db.pages.insert_one({'_id': 'page_one', 'body': html.format('Hello!')})
    await db.pages.insert_one({'_id': 'page_two', 'body': html.format('Goodbye.')})
    return db

async def page_handler(request):
    page_name = request.match_info.get('page_name')
    db = request.app['db']
    document = await db.pages.find_one(page_name)

    if not document:
        return web.HTTPNotFound(text='No page named {!r}'.format(page_name))

    return web.Response(body=document['body'].encode(), content_type='text/html')

loop = asyncio.get_event_loop()
db = loop.run_until_complete(setup_db())
app = web.Application()
app['db'] = db
app.router.add_get('/pages/{page_name}', page_handler)
web.run_app(app)
